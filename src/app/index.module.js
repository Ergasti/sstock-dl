(function() {
  'use strict';

  angular
    .module('sstockDl', ['ngSanitize', 'ui.router', 'ngMaterial', 'toastr', 'ngLodash']);

})();
