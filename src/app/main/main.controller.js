(function() {
  'use strict';

  angular
    .module('sstockDl')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($http, API, lodash) {
    var vm = this;
    vm.lbId = 0;
    vm.dlLinks = [];
    console.log(API);

    vm.imgIds = [];

    vm.onSubmit = function () {
      if (vm.lbId != 0) {
        $http.get(API + '/getLightboxImages?lightboxId=' + vm.lbId).then(function (res) {

          vm.imgIds = res.data.imgIds;

          var groupSize = 50;

          vm.groups = lodash.map(vm.imgIds, function(item, index){

            return index % groupSize === 0 ? vm.imgIds.slice(index, index + groupSize) : null;
            })
            .filter(function(item) { return item; });


        }).then(function () {
          lodash.each(vm.groups, function (group) {
            $http.get(API + '/getDlLinks', {params: {imgIds: JSON.stringify(group)}}).then(function (res2) {
              // vm.dlLinks += res2.data;
              var links = res2.data;
              lodash.each(links, function (link) {
                if (link.download) vm.dlLinks.push(link.download.url);
                else vm.dlLinks.push('https://www.shutterstock.com/pic.mhtml?id=' + link.image_id);
              });
            });
          })
        });
      } else {
        console.log('not 0');
      }
      vm.lbId = 0;
    }
  }
})();
