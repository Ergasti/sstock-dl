(function() {
  'use strict';

  angular
    .module('sstockDl')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
